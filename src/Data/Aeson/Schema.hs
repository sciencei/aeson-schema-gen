{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.Aeson.Schema where

import qualified Data.Vector as V
import Data.HashMap.Strict (HashMap, unionWith, mapMaybe, foldlWithKey', insert, singleton, insertWith)
import Data.Text (Text)
import Data.Scientific (isFloating,  toBoundedInteger)
import Data.Aeson.Types
import Data.List (nub)
import Data.Foldable (foldl')

data SchemaSettings = SchemaSettings
  { savedValues :: Int -- ^ number of distinct values to save for
                       --   a field before changing it to freeform
  } deriving (Show, Read, Eq)

defaultSchemaSettings :: SchemaSettings
defaultSchemaSettings = SchemaSettings { savedValues = 15 }

data Schema = Schema { settings :: SchemaSettings
                     , schema :: SValue
                     } deriving (Show, Read, Eq)

data SValue = SObject SObject
            | SArray SValue
            | SString SString
            | SInt SInt
            | SScientific
            | SBool
            | SAnyOf SAnyOf
            deriving (Show, Read, Eq)

tag :: SValue -> Text
tag (SObject _) = "object"
tag SScientific = "scientific"
tag (SInt _) = "integer"
tag (SString _) = "string"
tag SBool = "bool"
tag (SArray _) = "array of"
tag (SAnyOf _) = "any of"

instance Semigroup SchemaSettings where
  left <> right = SchemaSettings { savedValues = max (savedValues left) (savedValues right) }

instance Semigroup Schema where
  Schema lset SBool <> Schema rset SBool = Schema (lset <> rset) SBool
  Schema lset SScientific <> Schema rset SScientific = Schema (lset <> rset) SScientific
  Schema lset (SString l) <> Schema rset (SString r) = Schema (lset <> rset) . SString $ mappendPossEnum (lset <> rset) l r
  Schema lset (SInt l) <> Schema rset (SInt r) = Schema (lset <> rset) . SInt $ mappendPossEnum (lset <> rset) l r
  Schema lset (SArray l) <> Schema rset (SArray r) = Schema (settings sub) (SArray $ schema sub)
    where sub = (Schema lset l) <> (Schema rset r)
  Schema lset (SAnyOf l) <> Schema rset (SAnyOf r) = Schema settings . SAnyOf $ unionWith (mappendSVals settings) l r
    where settings = lset <> rset
  Schema lset (SAnyOf l) <> Schema rset r = Schema settings . SAnyOf $ insertWith (mappendSVals settings) (tag r) r l
    where settings = lset <> rset
  Schema lset (SObject lobj) <> Schema rset (SObject robj) = Schema settings $ SObject combined
    where
      settings = lset <> rset
      visitedl = fmap (,False) lobj
      visitedr = fmap (,False) robj
      combinedPrePrune = unionWith (\(l, _) (r, _) -> (mappendSVals settings <$> l <*> r, True)) visitedl visitedr
      combined = fmap (\case (v, False) -> makeOptional v
                             (v, True)  -> v)
                      combinedPrePrune
  Schema lset l <> Schema rset r = Schema settings . SAnyOf $ (insert (tag l) l $ singleton (tag r) r)
    where settings = lset <> rset

mappendSVals :: SchemaSettings -> SValue -> SValue -> SValue
mappendSVals settings l r = schema (Schema settings l <> Schema settings r)

mappendPossEnum :: Eq a => SchemaSettings -> PossEnum a -> PossEnum a -> PossEnum a
mappendPossEnum _ Any _ = Any
mappendPossEnum _ _ Any = Any
mappendPossEnum set (OneOf ls) (OneOf rs) =
    if length outs <= savedValues set then OneOf outs else Any
  where
    outs = nub $ ls <> rs

instance (Semigroup a) => Semigroup (OptionalTag a) where
  Required l <> Required r = Required $ l <> r
  Optional l <> Required r = Optional $ l <> r
  Required l <> Optional r = Optional $ l <> r

data OptionalTag a = Optional a
                   | Required a
                   deriving (Show, Read, Eq, Functor)

makeOptional :: OptionalTag a -> OptionalTag a
makeOptional (Required x) = Optional x
makeOptional op           = op

instance Applicative OptionalTag where
  (Optional f) <*> (Required x) = Optional (f x)
  (Optional f) <*> (Optional x) = Optional (f x)
  (Required f) <*> (Required x) = Required (f x)
  (Required f) <*> (Optional x) = Optional (f x)

  pure x = Required x

data PossEnum a = OneOf [a]
                | Any
                deriving (Show, Read, Eq)

type SObject = HashMap Text (OptionalTag SValue)

type SString = PossEnum Text

type SInt = PossEnum Int

type SAnyOf = HashMap Text SValue

toValue :: Schema -> Value
toValue (Schema _ sval) = Object $ singleton (tag sval) (toRep sval)
  where
    toRep SScientific = String "Any"
    toRep (SInt i) = possEnumToVal (Number . fromIntegral) i
    toRep (SString s) = possEnumToVal String s
    toRep SBool = String "Any"
    toRep (SObject obj) = Object $ foldlWithKey' (\acc k v -> uncurry insert (uncurry toValue' $ unwrap k v) acc) mempty obj
    toRep (SArray sub) = toRep sub
    toRep (SAnyOf ls) = Object $ fmap toRep ls
    annotate t k v = ((k <> "(" <> t <> ")" ), v)
    possEnumToVal _ Any = String "Any"
    possEnumToVal con (OneOf ls) = Array (con <$> V.fromList ls)
    unwrap k (Required v) = annotate "required" k v
    unwrap k (Optional v) = annotate "optional" k v
    toValue' k v = annotate (tag v) k (toRep v)

valueToSchema :: SchemaSettings -> Value -> Maybe Schema
valueToSchema settings (Number n)
  | isFloating n                 = Just . Schema settings $ SScientific
  | savedValues settings > 0     = Schema settings . SInt . OneOf . pure <$> toBoundedInteger n
  | otherwise                    = Just . Schema settings . SInt $ Any
valueToSchema settings (String t)
  | savedValues settings > 0     = Just . Schema settings . SString $ OneOf [t]
  | otherwise                    = Just . Schema settings . SString $ Any
valueToSchema settings (Bool _)  = Just . Schema settings $ SBool
valueToSchema settings (Object o) = Just . Schema settings . SObject
                                         $ mapMaybe (fmap (Required . schema) . valueToSchema settings) o
valueToSchema settings (Array a) = Just . Schema settings . SArray
                                        . schema
                                        . V.foldl' (<>) (Schema settings (SAnyOf mempty))
                                        $ V.mapMaybe (valueToSchema settings) a
valueToSchema _         Null     = Nothing
